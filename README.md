# FE NPM Public Registry

## How to install packages from this registry

TLDR;

```sh
echo @fe:registry=https://gitlab.gwdg.de/api/v4/projects/27465/packages/npm/ >> .npmrc
```

## How to push to this registry from GitLab CI

1. Follow [How to install packages from this registry](#how-to-install-packages-from-this-registry).
1. Include the npm publish template provided by the GitLab project in your `.gitlab-ci.yml`.
    ```yaml
    include:
    # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/npm.gitlab-ci.yml
      - template: npm.gitlab-ci.yml
    ```
1. Update the imported "publish" job in your `.gitlab-ci.yml` with authorization credentials.
    ```yaml
    publish:
      before_script:
        - echo //gitlab.gwdg.de/api/v4/projects/27465/packages/npm/:_authToken=${CI_JOB_TOKEN} >> .npmrc
    ```
